import { Col, Row } from "reactstrap";
import styled from "styled-components";

const ContenidoDisplayCalculadora = styled.div`
    border: 2px solid black;
    padding: 15px;
    font-size: 14px;
    font-weight: bold;
    text-align: right;
`;

export const Display = (props) => {
    return(
        <ContenidoDisplayCalculadora>
            <Row>
                <Col style={{textAlign: "initial"}}>
                    {props.operacion}
                </Col>
                <Col>
                    {props.numerosOperacion}
                </Col>
            </Row>
        </ContenidoDisplayCalculadora>
    )
}

export default Display;