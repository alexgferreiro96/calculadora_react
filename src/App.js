import { useState } from 'react';
import { Container } from 'reactstrap';
import styled from 'styled-components';
import './App.css';
import BotonesCalculadora from './BotonesCalculadora';
import Display from './Display';

const CalculadoraBody = styled.div`
  border: 2px solid red;
  padding: 15px;
  width: 25%;
  text-align: center;
`;

const App = () => {

  const [numerosCalculo, setNumerosCalculo] = useState([0]);
  const [operacionCompleta, setOperacionCompleta] = useState([]);
  const [flag,setFlag] = useState(false);

  const actualizaDisplay = (valor) => {
    if(!isNaN(valor) || valor === "."){
      pulsaUnNumero(valor);
    }else if(valor === "="){
      calculaResultadoFinal();
    }else{
      pulsaUnaOperacion(valor);
    }
  }

  const pulsaUnNumero = (valor) => {
    let arrayActualizado = flag === true ? [] : [...numerosCalculo];
    arrayActualizado.push(valor);
    if(arrayActualizado[0] === 0) arrayActualizado.shift();
    setNumerosCalculo(arrayActualizado);
    setFlag(false);
  }

  const pulsaUnaOperacion = (valor) => {
    let arrayActualizado = parseFloat([...numerosCalculo].join(""));
    setOperacionCompleta([arrayActualizado,valor]);
    setFlag(true);
  }

  const calculaResultadoFinal = () => {
    let arrayActualizado = parseFloat([...numerosCalculo].join(""));
    let resultOperation = 0;
    switch (operacionCompleta[1]) {
      case "+":
          resultOperation = operacionCompleta[0] + arrayActualizado;
        break;
      case "-":
          resultOperation = operacionCompleta[0] - arrayActualizado;
        break;
      case "X":
          resultOperation = operacionCompleta[0] * arrayActualizado;
        break;
      case "/":
          resultOperation = operacionCompleta[0] / arrayActualizado;
        break;
      default:
        break;
    }
    setNumerosCalculo(resultOperation.toString().split(""));
    setOperacionCompleta([]);
    setFlag(true);
  }

  return (
    <Container>
      <CalculadoraBody>
        <Display numerosOperacion={numerosCalculo} operacion={operacionCompleta[1]} />
        <BotonesCalculadora interaccionUsuario={actualizaDisplay} />
      </CalculadoraBody>
    </Container>
  );
}

export default App;
