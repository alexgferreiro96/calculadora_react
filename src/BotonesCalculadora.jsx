import { Button, Col, Container, Row } from "reactstrap";


const BotonesCalculadora = (props) => {

    const teclasParaBotones = [7,8,9,"/",4,5,6,"*",1,2,3,"-",0,".","=","+"];
    const botones = teclasParaBotones.map((el, index)=>{
        return(
            <Col key={index+"-col"}>
                <Button className={"btn-especial btn-block"} color={"secondary"} onClick={()=>props.interaccionUsuario(el)}>{el}</Button>
            </Col>
        );
    });    

    return(
        <Container>
            <Row xs={4}>
                {botones}
            </Row>
        </Container>
    )
}

export default BotonesCalculadora;